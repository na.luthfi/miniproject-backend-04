package main

import (
	"context"
	"log"
	"net"

	"gitlab.com/na.luthfi/miniproject-backend-04/author/config"
	"gitlab.com/na.luthfi/miniproject-backend-04/author/services"

	pb "gitlab.com/na.luthfi/miniproject-backend-04/author/proto"
	"google.golang.org/grpc"
)

type AuthorServer struct {
	service services.AuthorService
}

func NewAuthorServer(as services.AuthorService) *AuthorServer {
	return &AuthorServer{
		service: as,
	}
}

func (authorServer AuthorServer) FindAuthorByID(ctx context.Context, param *pb.AuthorInputID) (*pb.Author, error) {
	author, _ := authorServer.service.FindAuthorByID(param.Id)

	resp := &pb.Author{
		Id:    author.ID.String(),
		Name:  author.Name,
		Email: author.Email,
	}
	return resp, nil
}
func (authorServer AuthorServer) CreateAuthor(ctx context.Context, args *pb.AuthorInput) (res *pb.Author, err error) {
	author, err := authorServer.service.CreateAuthor(args.Name, args.Email)
	if err != nil {
		return res, err
	}

	res = &pb.Author{
		Id:    author.ID.String(),
		Name:  author.Name,
		Email: author.Email,
	}
	return res, nil
}

func main() {
	//create user server
	srv := grpc.NewServer()
	var authorServer AuthorServer
	authorServer.service = *services.NewAuthorService()

	//register and start user server

	pb.RegisterAuthorServiceServer(srv, authorServer)

	log.Println("Starting RPC server at", config.GRPCServerPort)

	listen, err := net.Listen("tcp", string(config.GRPCServerPort))

	if err != nil {
		log.Fatalf("Could not listen to %s: %v", config.GRPCServerPort, err)
	}

	log.Fatal("Server is listening", srv.Serve(listen))
}

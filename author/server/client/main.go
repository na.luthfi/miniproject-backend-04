package main

import (
	"context"
	"fmt"

	"gitlab.com/na.luthfi/miniproject-backend-04/author/config"
	pb "gitlab.com/na.luthfi/miniproject-backend-04/author/proto"
)

func main() {
	author := config.InitGRPCAuthorServiceClient()

	id := pb.AuthorInputID{
		Id: "222cb379-ed93-49bf-b0b4-17bb287c0c09",
	}

	result, err := author.FindAuthorByID(context.Background(), &id)

	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(result)

}

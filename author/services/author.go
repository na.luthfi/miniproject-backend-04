package services

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/na.luthfi/miniproject-backend-04/author/domain"
	"gitlab.com/na.luthfi/miniproject-backend-04/author/repository"
	"gitlab.com/na.luthfi/miniproject-backend-04/author/repository/inmemory"
	"gitlab.com/na.luthfi/miniproject-backend-04/author/storage"
)

// AuthorService is
type AuthorService struct {
	Query      repository.AuthorQuery
	Repository repository.AuthorRepository
}

// NewAuthorService is to create author services
func NewAuthorService() *AuthorService {
	var repo repository.AuthorRepository
	var query repository.AuthorQuery

	db := storage.NewAuthorStorage()
	query = inmemory.NewAuthorQueryInMemory(db)
	repo = inmemory.NewAuthorRepositoryInMemory(db)

	return &AuthorService{
		Query:      query,
		Repository: repo,
	}
}

// FindAuthorByID is to find author by id
func (service *AuthorService) FindAuthorByID(uid string) (domain.Author, error) {
	id, err := uuid.FromString(uid)
	if err != nil {
		return domain.Author{}, err
	}

	result := <-service.Query.FindAuthorByID(id)
	if result.Error != nil {
		// log.WithFields(log.Fields{"time": time.Now()}).Info("Ini error dari services")
		return domain.Author{}, result.Error
	}

	// log.WithFields(log.Fields{"time": time.Now()}).Info("No error")

	return result.Result.(domain.Author), nil
}

// CreateAuthor to create author
func (service *AuthorService) CreateAuthor(name, email string) (domain.Author, error) {
	// Process
	result, err := domain.CreateAuthor(name, email)
	if err != nil {
		return domain.Author{}, err
	}

	// Persist
	err = <-service.Repository.Save(result)
	if err != nil {
		return domain.Author{}, err
	}

	return *result, nil
}

package storage

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/na.luthfi/miniproject-backend-04/author/domain"
)

// AuthorStorage is to create author
type AuthorStorage struct {
	AuthorMap []domain.Author
}

// NewAuthorStorage s
func NewAuthorStorage() *AuthorStorage {
	author1, _ := domain.CreateAuthor("Luthfi", "luthfi.aida@kumparan.com")
	author2, _ := domain.CreateAuthor("Fajri", "fajri_fernanda@kumparan.com")

	author1.ID, _ = uuid.FromString("56ef5860-bbd3-4125-836f-6af8bc42dbe2")
	author2.ID, _ = uuid.FromString("222cb379-ed93-49bf-b0b4-17bb287c0c09")

	return &AuthorStorage{
		AuthorMap: []domain.Author{
			*author1, *author2,
		},
	}
}

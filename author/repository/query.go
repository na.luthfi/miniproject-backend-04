package repository

import uuid "github.com/satori/go.uuid"

// QueryResult is to wrap query result
type QueryResult struct {
	Result interface{}
	Error  error
}

type AuthorQuery interface {
	FindAuthorByID(uuid.UUID) <-chan QueryResult
}

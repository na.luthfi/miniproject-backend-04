package repository

import "gitlab.com/na.luthfi/miniproject-backend-04/author/domain"

// AuthorRepository is wrap contract author repository
type AuthorRepository interface {
	Save(name *domain.Author) <-chan error
}

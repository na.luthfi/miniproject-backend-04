package inmemory

import (
	"errors"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/na.luthfi/miniproject-backend-04/author/domain"
	"gitlab.com/na.luthfi/miniproject-backend-04/author/repository"
	"gitlab.com/na.luthfi/miniproject-backend-04/author/storage"
)

// AuthorQueryInMemory is author query implementation in memory
type AuthorQueryInMemory struct {
	Storage *storage.AuthorStorage
}

// NewAuthorQueryInMemory is to Create Instance AuthorQueryInMemory
func NewAuthorQueryInMemory(storage *storage.AuthorStorage) repository.AuthorQuery {
	return &AuthorQueryInMemory{Storage: storage}
}

// FindAuthorByID is to find author by id
func (query *AuthorQueryInMemory) FindAuthorByID(id uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		author := domain.Author{}
		for _, item := range query.Storage.AuthorMap {
			if id == item.ID {
				author = item
			}
		}

		if author.Name == "" {
			result <- repository.QueryResult{Error: errors.New("Author not found")}
		} else {
			result <- repository.QueryResult{Result: author}
		}

		close(result)
	}()

	return result
}

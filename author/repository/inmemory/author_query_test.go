package inmemory

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/na.luthfi/miniproject-backend-04/author/domain"
	"gitlab.com/na.luthfi/miniproject-backend-04/author/repository"
	"gitlab.com/na.luthfi/miniproject-backend-04/author/storage"
)

func TestCanFindAuthorByID(t *testing.T) {
	// Given
	done := make(chan bool)

	author1UID, _ := uuid.NewV4()
	author1 := domain.Author{
		ID:    author1UID,
		Name:  "Foobar",
		Email: "foobar@foobar.com",
	}

	author2UID, _ := uuid.NewV4()
	author2 := domain.Author{
		ID:    author2UID,
		Name:  "Foobar 2",
		Email: "foobar2@foobar.com",
	}

	storage := storage.AuthorStorage{
		AuthorMap: []domain.Author{
			author1, author2,
		},
	}

	query := NewAuthorQueryInMemory(&storage)
	result := repository.QueryResult{}

	// When
	go func() {
		result, _ = <-query.FindAuthorByID(author1UID)
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, author1, result.Result.(domain.Author))
}

package inmemory

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/na.luthfi/miniproject-backend-04/author/domain"
	"gitlab.com/na.luthfi/miniproject-backend-04/author/storage"
)

func TestCanSaveAuthorInRepository(t *testing.T) {
	// Given
	done := make(chan bool)
	authorUID, _ := uuid.NewV4()
	author := domain.Author{
		ID:    authorUID,
		Name:  "Foobar",
		Email: "foobar@foobar.com",
	}

	db := storage.NewAuthorStorage()
	repo := NewAuthorRepositoryInMemory(db)

	// When
	var err error
	go func() {
		err = <-repo.Save(&author)
		done <- true
	}()

	<-done

	// Then
	assert.Nil(t, err)
	assert.Equal(t, 3, len(db.AuthorMap))
}

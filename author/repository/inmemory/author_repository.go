package inmemory

import (
	"gitlab.com/na.luthfi/miniproject-backend-04/author/domain"
	"gitlab.com/na.luthfi/miniproject-backend-04/author/repository"
	"gitlab.com/na.luthfi/miniproject-backend-04/author/storage"
)

// AuthorRepositoryInMemory is author repository implementation in memory
type AuthorRepositoryInMemory struct {
	Storage *storage.AuthorStorage
}

// NewAuthorRepositoryInMemory is to create AuthorRepositoryInMemory instance
func NewAuthorRepositoryInMemory(storage *storage.AuthorStorage) repository.AuthorRepository {
	return &AuthorRepositoryInMemory{Storage: storage}
}

// Save is for save author
func (repo *AuthorRepositoryInMemory) Save(author *domain.Author) <-chan error {
	result := make(chan error)

	go func() {
		repo.Storage.AuthorMap = append(repo.Storage.AuthorMap, *author)

		result <- nil
		close(result)
	}()

	return result
}

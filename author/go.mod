module gitlab.com/na.luthfi/miniproject-backend-04/author

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b
	github.com/stretchr/testify v1.3.0
	gitlab.com/na.luthfi/miniproject-backend-04/article v0.0.0-20190727081221-dabfe54f7394 // indirect
	gitlab.com/na.luthfi/miniproject-backend-04/graphql v0.0.0-20190727081221-dabfe54f7394
	google.golang.org/grpc v1.22.1
)

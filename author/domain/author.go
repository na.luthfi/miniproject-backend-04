package domain

import uuid "github.com/satori/go.uuid"

// Author wrapper
type Author struct {
	ID    uuid.UUID
	Name  string
	Email string
}

// CreateAuthor is to create author
func CreateAuthor(name, email string) (*Author, error) {
	uid, err := uuid.NewV4()
	if err != nil {
		return &Author{}, err
	}

	return &Author{
		ID:    uid,
		Name:  name,
		Email: email,
	}, nil
}

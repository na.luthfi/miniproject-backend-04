package config

import (
	"log"

	pb "gitlab.com/na.luthfi/miniproject-backend-04/author/proto"
	"google.golang.org/grpc"
)

func InitGRPCAuthorServiceClient() pb.AuthorServiceClient {
	port := "localhost:9200"
	conn, err := grpc.Dial(port, grpc.WithInsecure())

	if err != nil {
		log.Fatal("Could not connect to", port, err)
	}

	return pb.NewAuthorServiceClient(conn)
}

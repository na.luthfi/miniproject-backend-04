package main

import (
	"io/ioutil"
	"net/http"
	"time"

	graphql "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	log "github.com/sirupsen/logrus"
	"gitlab.com/na.luthfi/miniproject-backend-04/graphql/resolver"
	"gitlab.com/na.luthfi/miniproject-backend-04/graphql/services"
)

type query struct{}

func (*query) Hello() string {
	return "Hello, world!"
}

func main() {
	s, err := getSchema("./schema/schema.graphql")
	if err != nil {
		panic(err)
	}

	resolver := &resolver.Resolver{ArticleService: services.NewArticleService(), AuthorService: services.NewAuthorService()}
	schema := graphql.MustParseSchema(s, resolver)

	//
	mux := http.NewServeMux()

	mux.Handle("/query", &relay.Handler{Schema: schema})

	log.WithFields(log.Fields{"time": time.Now()}).Info("starting server")
	log.Fatal(http.ListenAndServe("localhost:9000", logged(mux)))
}

func getSchema(path string) (string, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}

	return string(b), nil
}

// logging middleware
func logged(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now().UTC()

		next.ServeHTTP(w, r)

		log.WithFields(log.Fields{
			"path":    r.RequestURI,
			"IP":      r.RemoteAddr,
			"elapsed": time.Now().UTC().Sub(start),
		}).Info()
	})
}

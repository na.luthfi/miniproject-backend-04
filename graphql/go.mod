module gitlab.com/na.luthfi/miniproject-backend-04/graphql

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/graph-gophers/graphql-go v0.0.0-20190724201507-010347b5f9e6
	github.com/pkg/errors v0.8.1
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.3.0
	gitlab.com/na.luthfi/miniproject-backend-04/author v0.0.0-20190727110741-6612ec740fac
	google.golang.org/grpc v1.22.1

)

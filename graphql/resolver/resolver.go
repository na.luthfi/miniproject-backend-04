package resolver

import (
	pb "gitlab.com/na.luthfi/miniproject-backend-04/graphql/proto"
)

// Resolver is to resolve
type Resolver struct {
	// Ganti koneksi ke grpc
	// ------------------------------------------
	AuthorService pb.AuthorServiceClient

	//Connection to grpc service
	ArticleService pb.ArticleServiceClient
}

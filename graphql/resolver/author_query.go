package resolver

import (
	"context"

	"github.com/pkg/errors"
	pb "gitlab.com/na.luthfi/miniproject-backend-04/graphql/proto"
)

// FindAuthorByID is to find author by id
func (r *Resolver) FindAuthorByID(ctx context.Context, args struct{ ID string }) (*AuthorResolver, error) {
	author, err := r.AuthorService.FindAuthorByID(ctx, &pb.AuthorInputID{Id: args.ID})

	if err != nil {
		return nil, errors.Wrap(err, "FindAuthorByID")
	}

	result := AuthorResolver{m: *author}
	return &result, nil
}

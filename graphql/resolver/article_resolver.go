package resolver

import (
	"context"

	pb "gitlab.com/na.luthfi/miniproject-backend-04/graphql/proto"
)

// ArticleResolver is to resolve article
type ArticleResolver struct {
	// m domain.Article
	// n domain.Author
	m pb.Article
	n pb.Author
}

// ID to resolve id
func (r *ArticleResolver) ID(ctx context.Context) *string {
	result := r.m.Id
	return &result
}

// Name is to resolve
func (r *ArticleResolver) Name(ctx context.Context) *string {
	return &r.m.Name
}

// Slug is to resolve slug name
func (r *ArticleResolver) Slug(ctx context.Context) *string {
	return &r.m.Slug
}

// Body is to resolve body
func (r *ArticleResolver) Body(ctx context.Context) *string {
	return &r.m.Body
}

//CreatedAt is to resolve createdat
func (r *ArticleResolver) CreatedAt(ctx context.Context) *string {
	return &r.m.CreatedAt
}

//UpdatedAt is to resolve updatedat
func (r *ArticleResolver) UpdatedAt(ctx context.Context) *string {
	return &r.m.UpdatedAt
}

// CreatedAt is to  created
// func (r *ArticleResolver) CreatedAt(ctx context.Context) *string {
// 	result := r.m.CreatedAt.String()
// 	return &result
// }

// UpdatedAt is
// func (r *ArticleResolver) UpdatedAt(ctx context.Context) *string {
// 	result := r.m.UpdatedAt.String()
// 	return &result
// }

//Author is
func (r *ArticleResolver) Author(ctx context.Context) *AuthorResolver {
	// result := r.n
	// return &AuthorResolver{
	// 	m: result,
	// }
	result := pb.Author{
		// Id:    r.n.ID.String(),
		Id:    r.n.Id,
		Name:  r.n.Name,
		Email: r.n.Email,
	}
	return &AuthorResolver{
		m: result,
	}

}

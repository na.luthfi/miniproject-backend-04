package resolver

import (
	"context"

	// "gitlab.com/na.luthfi/miniproject-backend-04/graphql/domain"
	pb "gitlab.com/na.luthfi/miniproject-backend-04/graphql/proto"
)

// AuthorResolver is to resolve author
type AuthorResolver struct {
	// m domain.Author
	m        pb.Author
	articles []pb.Article
}

// ID to resolve id
func (r *AuthorResolver) ID(ctx context.Context) *string {
	// result := r.m.ID.String()
	result := r.m.Id
	return &result
}

// Name is to resolve
func (r *AuthorResolver) Name(ctx context.Context) *string {
	return &r.m.Name
}

// Email is to resolve email name
func (r *AuthorResolver) Email(ctx context.Context) *string {
	return &r.m.Email
}

//Articles is to resolve articles
func (r *AuthorResolver) Articles(ctx context.Context) *[]*ArticleResolver {
	var articless = make([]*ArticleResolver, len(r.articles))

	for k, v := range r.articles {
		articless[k] = &ArticleResolver{
			m: v,
		}
	}
	return &articless
}

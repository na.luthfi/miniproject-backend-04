package resolver

import (
	"context"

	pb "gitlab.com/na.luthfi/miniproject-backend-04/graphql/proto"
)

// ArticleInput input result
type ArticleInput struct {
	Name *string
	Body *string
}

type ArticleInputWithAuthorID struct {
	Name     *string
	Body     *string
	AuthorID *string
}

// CreateArticle to create
func (r *Resolver) CreateArticle(ctx context.Context, args struct{ Article ArticleInput }) (*ArticleResolver, error) {
	article := &pb.ArticleInput{
		Name: *args.Article.Name,
		Body: *args.Article.Body,
	}
	result, err := r.ArticleService.CreateArticle(ctx, article)
	if err != nil {
		return nil, err
	}

	resolver := ArticleResolver{
		m: *result}

	return &resolver, nil
}

//CreateArticleWithAuthorID is to create article with author id
func (r *Resolver) CreateArticleWithAuthorID(ctx context.Context, args struct{ Article ArticleInputWithAuthorID }) (*ArticleResolver, error) {
	article := &pb.ArticleInputWithAuthorId{
		Name:     *args.Article.Name,
		Body:     *args.Article.Body,
		AuthorId: *args.Article.AuthorID,
	}
	result, err := r.ArticleService.CreateArticleWithAuthorId(ctx, article)
	if err != nil {
		return nil, err
	}

	resolver := ArticleResolver{m: *result}

	return &resolver, nil
}

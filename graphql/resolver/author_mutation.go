package resolver

import (
	"context"

	pb "gitlab.com/na.luthfi/miniproject-backend-04/graphql/proto"
)

// AuthorInput input result
type AuthorInput struct {
	Name  *string
	Email *string
}

// CreateAuthor to create
func (r *Resolver) CreateAuthor(ctx context.Context, args struct{ Author AuthorInput }) (*AuthorResolver, error) {
	// result, err := r.AuthorService.CreateAuthor(*args.Author.Name, *args.Author.Email)
	result, err := r.AuthorService.CreateAuthor(ctx, &pb.AuthorInput{Name: *args.Author.Name, Email: *args.Author.Email})
	if err != nil {
		return nil, err
	}

	resolver := AuthorResolver{m: *result}

	return &resolver, nil
}

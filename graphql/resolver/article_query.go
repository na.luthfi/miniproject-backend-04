package resolver

import (
	"context"

	"github.com/pkg/errors"
	pb "gitlab.com/na.luthfi/miniproject-backend-04/graphql/proto"
)

// GetArticles is to get articles
func (r *Resolver) GetArticles(ctx context.Context) (*[]*ArticleResolver, error) {
	// Ngobrol ke grpc
	// ------------------------------------------
	empty := pb.Empty{}
	articleListResponse, err := r.ArticleService.GetArticles(ctx, &empty)
	articles := articleListResponse.Article
	if err != nil {
		return nil, err
	}

	result := make([]*ArticleResolver, len(articles))
	for i, v := range articles {

		result[i] = &ArticleResolver{
			m: pb.Article{
				Name:      v.Name,
				Id:        v.Id,
				Slug:      v.Slug,
				Body:      v.Body,
				CreatedAt: v.CreatedAt,
				UpdatedAt: v.UpdatedAt,
			},
		}
	}
	return &result, err
}

// FindArticleByID is to find article by id
func (r *Resolver) FindArticleByID(ctx context.Context, args struct{ ID string }) (*ArticleResolver, error) {
	input := &pb.ArticleIdInput{
		ArticleId: args.ID,
	}
	articleResult, err := r.ArticleService.FindArticleById(ctx, input)

	if err != nil {
		return nil, errors.Wrap(err, "FindArticleByID")
	}
	var article = pb.Article{
		Id:        articleResult.Id,
		Name:      articleResult.Name,
		Slug:      articleResult.Slug,
		Body:      articleResult.Body,
		CreatedAt: articleResult.CreatedAt,
		UpdatedAt: articleResult.UpdatedAt,
	}

	result := ArticleResolver{m: article}
	return &result, nil
}

// GetArticlesWithAuthor is to get articles
func (r *Resolver) GetArticlesWithAuthor(ctx context.Context) (*[]*ArticleResolver, error) {
	// Ngobrol ke grpc
	// ------------------------------------------
	empty := pb.Empty{}
	articleListResponse, err := r.ArticleService.GetArticles(ctx, &empty)
	articles := articleListResponse.Article
	if err != nil {
		return nil, err
	}

	result := make([]*ArticleResolver, len(articles))

	for i, v := range articles {
		author, err := r.AuthorService.FindAuthorByID(ctx, &pb.AuthorInputID{Id: v.UserId})

		if err != nil {
			return nil, err
		}

		result[i] = &ArticleResolver{
			m: pb.Article{
				Name:      v.Name,
				Id:        v.Id,
				Slug:      v.Slug,
				Body:      v.Body,
				CreatedAt: v.CreatedAt,
				UpdatedAt: v.UpdatedAt,
			},
			n: pb.Author{
				Id:    v.UserId,
				Name:  author.Name,
				Email: author.Email,
			},
		}
	}
	return &result, err
}

// GetArticlesByAuthorId
func (r *Resolver) GetArticlesByAuthorId(ctx context.Context, args struct{ Id string }) (*AuthorResolver, error) {
	authorIdInput := &pb.AuthorIdInput{
		AuthorId: args.Id,
	}
	authorInputId := &pb.AuthorInputID{
		Id: args.Id,
	}

	authorResult, _ := r.AuthorService.FindAuthorByID(ctx, authorInputId)
	articleListResponse, err := r.ArticleService.GetArticlesByAuthorId(context.Background(), authorIdInput)

	if err != nil {
		return nil, err
	}
	articlesResult := make([]pb.Article, len(articleListResponse.Article))
	for k, v := range articleListResponse.Article {
		articlesResult[k] = pb.Article{
			Name:      v.Name,
			Id:        v.Id,
			Slug:      v.Slug,
			Body:      v.Body,
			CreatedAt: v.CreatedAt,
			UpdatedAt: v.UpdatedAt,
		}
	}
	result := AuthorResolver{
		m:        *authorResult,
		articles: articlesResult,
	}
	return &result, err
}

package repository

import "gitlab.com/na.luthfi/miniproject-backend-04/article/domain"

// ArticleRepository is wrap contract article repository
type ArticleRepository interface {
	Save(article *domain.Article) <-chan error
}

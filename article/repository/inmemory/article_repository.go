package inmemory

import (
	"gitlab.com/na.luthfi/miniproject-backend-04/article/domain"
	"gitlab.com/na.luthfi/miniproject-backend-04/article/repository"
	"gitlab.com/na.luthfi/miniproject-backend-04/article/storage"
)

// ArticleRepositoryInMemory is article repository implementation in memory
type ArticleRepositoryInMemory struct {
	Storage *storage.ArticleStorage
}

// NewArticleRepositoryInMemory is to create ArticleRepositoryInMemory instance
func NewArticleRepositoryInMemory(storage *storage.ArticleStorage) repository.ArticleRepository {
	return &ArticleRepositoryInMemory{Storage: storage}
}

// Save is for save article
func (repo *ArticleRepositoryInMemory) Save(article *domain.Article) <-chan error {
	result := make(chan error)

	go func() {
		repo.Storage.ArticleMap = append(repo.Storage.ArticleMap, *article)

		result <- nil
		close(result)
	}()

	return result
}

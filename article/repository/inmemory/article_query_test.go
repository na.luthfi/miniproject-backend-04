package inmemory

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/na.luthfi/miniproject-backend-04/article/domain"
	"gitlab.com/na.luthfi/miniproject-backend-04/article/repository"
	"gitlab.com/na.luthfi/miniproject-backend-04/article/storage"
)

func TestCanGetAllArticle(t *testing.T) {
	// Given
	done := make(chan bool)

	article1UID, _ := uuid.NewV4()
	article1 := domain.Article{
		ID:     article1UID,
		Name:   "Foobar",
		Slug:   "foobar",
		Body:   "body article",
		Status: domain.PUBLISHED,
	}

	article2UID, _ := uuid.NewV4()
	article2 := domain.Article{
		ID:     article2UID,
		Name:   "Foobar 2",
		Slug:   "foobar-2",
		Body:   "body article 2",
		Status: domain.PUBLISHED,
	}

	storage := storage.ArticleStorage{
		ArticleMap: []domain.Article{
			article1, article2,
		},
	}

	query := NewArticleQueryInMemory(&storage)
	result := repository.QueryResult{}

	// When
	go func() {
		result = <-query.GetArticles()
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, storage.ArticleMap, result.Result.([]domain.Article))
}

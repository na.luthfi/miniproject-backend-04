package inmemory

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/na.luthfi/miniproject-backend-04/article/domain"
	"gitlab.com/na.luthfi/miniproject-backend-04/article/storage"
)

func TestCanSaveArticleInRepository(t *testing.T) {
	// Given
	done := make(chan bool)
	articleUID, _ := uuid.NewV4()
	article := domain.Article{
		ID:     articleUID,
		Name:   "Foobar",
		Slug:   "foobar",
		Body:   "body article",
		Status: domain.PUBLISHED,
	}

	db := storage.NewArticleStorage()
	repo := NewArticleRepositoryInMemory(db)

	// When
	var err error
	go func() {
		err = <-repo.Save(&article)
		done <- true
	}()

	<-done

	// Then
	assert.Nil(t, err)
	assert.Equal(t, 3, len(db.ArticleMap))
}

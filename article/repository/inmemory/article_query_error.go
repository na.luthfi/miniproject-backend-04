package inmemory

// ArticleQueryError is article query error definition
type ArticleQueryError struct {
	Code int
}

const (
	// ArticleQueryErrorArticleNotFound indicicate if article is not found
	ArticleQueryErrorArticleNotFound = iota
)

// Error is error implementation
func (e ArticleQueryError) Error() string {
	switch e.Code {
	case ArticleQueryErrorArticleNotFound:
		return "Article is not found"
	default:
		return "Undefined article error code"
	}
}

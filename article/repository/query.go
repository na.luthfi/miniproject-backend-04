package repository

import uuid "github.com/satori/go.uuid"

// QueryResult is to wrap query result
type QueryResult struct {
	Result interface{}
	Error  error
}

// ArticleQuery is contract for article query
type ArticleQuery interface {
	// single
	GetArticles() <-chan QueryResult
	FindArticleByID(uuid.UUID) <-chan QueryResult
	GetArticlesByAuthorID(uuid.UUID) <-chan QueryResult
}

type AuthorQuery interface {
	FindAuthorByID(uuid.UUID) <-chan QueryResult
}

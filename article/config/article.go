package config

import (
	"log"

	pb "gitlab.com/na.luthfi/miniproject-backend-04/article/proto"
	"google.golang.org/grpc"
)

func InitGRPCArticleServiceClient() pb.ArticleServiceClient {
	port := "localhost:9100"
	conn, err := grpc.Dial(port, grpc.WithInsecure())

	if err != nil {
		log.Fatal("Could not connect to", port, err)
	}

	return pb.NewArticleServiceClient(conn)
}

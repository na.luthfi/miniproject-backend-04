package config

type PORT string

const (
	GRPCServerPort PORT = ":9100"
)

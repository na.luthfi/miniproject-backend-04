module gitlab.com/na.luthfi/miniproject-backend-04/article

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b
	github.com/stretchr/testify v1.3.0
	gitlab.com/na.luthfi/miniproject-backend-04/graphql v0.0.0-20190726084727-a8d14c5faa3a // indirect
	google.golang.org/grpc v1.22.1
)

package main

import (
	"context"
	"log"
	"net"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/na.luthfi/miniproject-backend-04/article/config"
	"gitlab.com/na.luthfi/miniproject-backend-04/article/services"

	pb "gitlab.com/na.luthfi/miniproject-backend-04/article/proto"
	"google.golang.org/grpc"
)

type ArticleServer struct {
	service services.ArticleService
}

func NewArticleServer(as services.ArticleService) *ArticleServer {
	return &ArticleServer{
		service: as,
	}
}

func (articleServer ArticleServer) GetArticles(ctx context.Context, empty *pb.Empty) (*pb.ArticleListResponse, error) {
	articlesFromService, _ := articleServer.service.GetAllArticles()
	var articles []*pb.Article
	for _, v := range articlesFromService {
		a := &pb.Article{
			Id:        v.ID.String(),
			Name:      v.Name,
			Slug:      v.Slug,
			Body:      v.Body,
			UserId:    v.UserID.String(),
			UpdatedAt: v.UpdatedAt.String(),
		}
		articles = append(articles, a)
	}

	resp := &pb.ArticleListResponse{
		Article: articles,
	}
	return resp, nil
}
func (articleServer ArticleServer) CreateArticle(ctx context.Context, args *pb.ArticleInput) (res *pb.Article, err error) {
	articleFromService, _ := articleServer.service.CreateArticle(args.Name, args.Body)
	res = &pb.Article{
		Id:        articleFromService.ID.String(),
		Name:      articleFromService.Name,
		Slug:      articleFromService.Slug,
		Body:      articleFromService.Body,
		UserId:    articleFromService.UserID.String(),
		CreatedAt: articleFromService.CreatedAt.String(),
		UpdatedAt: articleFromService.UpdatedAt.String(),
	}

	return res, nil
}
func (articleServer ArticleServer) CreateArticleWithAuthorId(context context.Context, args *pb.ArticleInputWithAuthorId) (res *pb.Article, err error) {
	authorId, _ := uuid.FromString(args.AuthorId)
	articleFromService, err := articleServer.service.CreateArticleWithAuthorId(args.Name, args.Body, authorId)
	if err != nil {
		return nil, err
	}
	res = &pb.Article{
		Id:        articleFromService.ID.String(),
		Name:      articleFromService.Name,
		Slug:      articleFromService.Slug,
		Body:      articleFromService.Body,
		UserId:    articleFromService.UserID.String(),
		CreatedAt: articleFromService.CreatedAt.String(),
		UpdatedAt: articleFromService.UpdatedAt.String(),
	}

	return res, nil
}

func (articleServer ArticleServer) FindArticleById(context context.Context, args *pb.ArticleIdInput) (res *pb.Article, err error) {

	articleFromService, err := articleServer.service.FindArticleByID(args.ArticleId)
	if err != nil {
		return nil, err
	}
	res = &pb.Article{
		Id:        articleFromService.ID.String(),
		Name:      articleFromService.Name,
		Slug:      articleFromService.Slug,
		UserId:    articleFromService.UserID.String(),
		CreatedAt: articleFromService.CreatedAt.String(),
		UpdatedAt: articleFromService.UpdatedAt.String(),
	}
	return res, nil
}

func (articleServer ArticleServer) GetArticlesByAuthorId(ctx context.Context, args *pb.AuthorIdInput) (*pb.ArticleListResponse, error) {
	articlesFromService, _ := articleServer.service.GetArticlesByAuthorID(args.AuthorId)
	var articles []*pb.Article
	for _, v := range articlesFromService {
		a := &pb.Article{
			Id:        v.ID.String(),
			Name:      v.Name,
			Slug:      v.Slug,
			Body:      v.Body,
			UserId:    v.UserID.String(),
			CreatedAt: v.CreatedAt.String(),
			UpdatedAt: v.UpdatedAt.String(),
		}
		articles = append(articles, a)
	}

	resp := &pb.ArticleListResponse{
		Article: articles,
	}
	return resp, nil
}

func main() {
	//create user server
	srv := grpc.NewServer()
	var articleServer ArticleServer
	articleServer.service = *services.NewArticleService()

	//register and start user server

	pb.RegisterArticleServiceServer(srv, articleServer)

	log.Println("Starting RPC server at", config.GRPCServerPort)

	listen, err := net.Listen("tcp", string(config.GRPCServerPort))

	if err != nil {
		log.Fatalf("Could not listen to %s: %v", config.GRPCServerPort, err)
	}

	log.Fatal("Server is listening", srv.Serve(listen))
}

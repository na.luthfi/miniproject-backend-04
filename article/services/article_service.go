package services

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/na.luthfi/miniproject-backend-04/article/domain"
	"gitlab.com/na.luthfi/miniproject-backend-04/article/repository"
	"gitlab.com/na.luthfi/miniproject-backend-04/article/repository/inmemory"
	"gitlab.com/na.luthfi/miniproject-backend-04/article/storage"
)

// ArticleService is
type ArticleService struct {
	Query      repository.ArticleQuery
	Repository repository.ArticleRepository
}

// NewArticleService is to create article services
func NewArticleService() *ArticleService {
	var repo repository.ArticleRepository
	var query repository.ArticleQuery

	db := storage.NewArticleStorage()
	query = inmemory.NewArticleQueryInMemory(db)
	repo = inmemory.NewArticleRepositoryInMemory(db)

	return &ArticleService{
		Query:      query,
		Repository: repo,
	}
}

// GetAllArticles is to find all articles
func (service *ArticleService) GetAllArticles() ([]domain.Article, error) {
	// Process
	result := <-service.Query.GetArticles()
	if result.Error != nil {
		return []domain.Article{}, result.Error
	}

	// Return
	return result.Result.([]domain.Article), nil
}

// FindArticleByID is to find article by id
func (service *ArticleService) FindArticleByID(uid string) (domain.Article, error) {
	id, err := uuid.FromString(uid)
	if err != nil {
		return domain.Article{}, err
	}

	result := <-service.Query.FindArticleByID(id)
	if result.Error != nil {
		// log.WithFields(log.Fields{"time": time.Now()}).Info("Ini error dari services")
		return domain.Article{}, result.Error
	}

	// log.WithFields(log.Fields{"time": time.Now()}).Info("No error")

	return result.Result.(domain.Article), nil
}

// CreateArticle to create article
func (service *ArticleService) CreateArticle(name, body string) (domain.Article, error) {
	// Process
	result, err := domain.CreateArticle(name, body)
	if err != nil {
		return domain.Article{}, err
	}
	// Persist
	err = <-service.Repository.Save(result)
	if err != nil {
		return domain.Article{}, err
	}

	return *result, nil
}

func (service *ArticleService) CreateArticleWithAuthorId(name, body string, authorId uuid.UUID) (domain.Article, error) {
	// Process
	result, err := domain.CreateArticle(name, body)
	if err != nil {
		return domain.Article{}, err
	}
	// insert authorid in article
	result.AttachUserID(authorId)
	// Persist
	err = <-service.Repository.Save(result)
	if err != nil {
		return domain.Article{}, err
	}

	return *result, nil
}

func (service *ArticleService) GetArticlesByAuthorID(authorId string) ([]domain.Article, error) {
	// Process
	authorIdUUID, _ := uuid.FromString(authorId)
	result := <-service.Query.GetArticlesByAuthorID(authorIdUUID)
	if result.Error != nil {
		return []domain.Article{}, result.Error
	}

	// Return
	return result.Result.([]domain.Article), nil
}

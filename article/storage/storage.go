package storage

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/na.luthfi/miniproject-backend-04/article/domain"
)

// ArticleStorage is article storage in memory
type ArticleStorage struct {
	ArticleMap []domain.Article
}

// NewArticleStorage is to create article storage
func NewArticleStorage() *ArticleStorage {
	article1, _ := domain.CreateArticle("Pelita Harapan", "Ini pelita harapan body")
	article2, _ := domain.CreateArticle("Jantung Harapan", "Ini jantung harapan body")

	article1.UserID, _ = uuid.FromString("222cb379-ed93-49bf-b0b4-17bb287c0c09")
	article2.UserID, _ = uuid.FromString("56ef5860-bbd3-4125-836f-6af8bc42dbe2")
	return &ArticleStorage{
		ArticleMap: []domain.Article{
			*article1, *article2,
		},
	}
}
